const InternList = [
    { 'ID': "@96232199577161728", 'Name': "Phil" },
    { 'ID': "@484101150317084674", 'Name': "Razan" },
    { 'ID': "@483587614331502602", 'Name': "Patricia" },
    { 'ID': "@485774302340186114", 'Name': "Abdullah" },
    { 'ID': "@483626767912009728", 'Name': "Mario" },
    { 'ID': "@483586684823273502", 'Name': "Ala'a" },
    { 'ID': "@484347979260755969", 'Name': "Mustafa" },
    { 'ID': "@319104397898678273", 'Name': "Imad" },
    { 'ID': "@483885495395549197", 'Name': "Adel" },
    { 'ID': "@483595098865795082", 'Name': "Ahmad" },
    { 'ID': "@483529331264454657", 'Name': "Sarkis" },
    { 'ID': "@369821182456758272", 'Name': "Yazid" },
    { 'ID': "@483551765229207572", 'Name': "Mireille" },
    { 'ID': "@484334504090009612", 'Name': "Bassell" },
    { 'ID': "@483617873160437761", 'Name': "HusseinHussein" },
    { 'ID': "@483571415161765894", 'Name': "Hussein" },
    { 'ID': "@90187714095226880", 'Name': "Tester" },
];

const ScheduleConfig = {
    'Morning': {
        'time': [15,56],
        'msg': "" // Todo
    },
    'Watch': {
        'time': [13,45],
        'msg': "" // Todo
    },
    'Afternoon': {
        'time': [16,30],
        'msg': "" // Todo
    },
};

const ChannelsID = {
    'General': '391931371787911168',
    'Cohort3': '481412258979643433'
};

let Active = false;
// States
module.exports = {
    isActive: () => {
        return Active;
    },

    SetActiveState: () => {
        Active = Active ? false : true;
    },

    // Ressources
    GetChannels: () => {
        return ChannelsID;
    },

    GetInternSizeList: () => {
        return InternList.length;
    },


    // Positions
    GetInternID: (pos) => {
        return InternList[pos].ID;
    },

    GetInternName: (pos) => {
        return InternList[pos].Name;
    },

    /**
     * Returns an index for a given name
     * @param {string} name an intern name
     * @return number the index of the intern 
     */
    GetIndexByName: (name) => {
        return InternList.findIndex(intern => intern.Name === name);
    },

    /**
     * Returns an index for a given id
     * @param {string} id an intern id
     * @return number the index of the intern 
     */
    getIndexbyID: (id) => {
        return InternList.findIndex(intern => intern.ID === id);
    },

    GetScheduleConfig: () => {
        return ScheduleConfig;
    }
}