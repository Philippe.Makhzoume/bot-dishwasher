const base = require('./base.js');

// https://stackoverflow.com/questions/872310/javascript-swap-array-elements
function multiSwap(arr, oldPos, newPos) {
    /* argument immutable if string */
    if (arr.split)
        return multiSwap(arr.split(""), oldPos, newPos).join("");

    var diff = [];
    for (let i in oldPos)
        diff[oldPos[i]] = arr[newPos[i]];

    return Object.assign(arr, diff);
}

function simpleSwap(pos)  {
    let arr = base.GetArrayLock();
    multiSwap(arr, [pos[0], pos[1]], [pos[1], pos[0]]);
};

module.exports = {
    swapByName: (names) => {
        let pos1, pos2;
        pos1 = base.GetPosByName(names[0]);
        pos2 = base.GetPosByName(names[1]);

        simpleSwap([pos1, pos2]);

        console.log('Debug : simpleSwap Occured');
        console.log(base.GetArrayLock());
    },

    swapByID: (ids) => {
        let pos1, pos2;
        pos1 = base.GetPosByID(ids[0]);
        pos2 = base.GetPosByID(ids[1]);

        console.log(pos1);
        console.log(pos2);

        simpleSwap([pos1, pos2]);

        console.log('Debug : SwapByID Occured');
        console.log(base.GetArrayLock());
    }
}
