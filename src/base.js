const rsc = require('./ressources');

let ArrayLock = [];
let TodayID = 0;

// Fisher-Yates Algorithm
function shuffle(array) {
    var m = array.length,
        t, i;
    // While there remain elements to shuffle…
    while (m) {
        // Pick a remaining element…
        i = Math.floor(Math.random() * m--);

        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }

    return array;
}

function HandleLimit() {
    TodayID = 0; // Renew List
    /*
        Double Shuffle > Hack Fix (Recurrence)
    */
    ArrayLock = shuffle(shuffle(ArrayLock));
    // debug
    console.log("Limit Reached Creating new datas");
    console.log(ArrayLock);
}

function GetIDByPos(pos) {
    return rsc.GetInternID(ArrayLock[pos]);
};

// Start-up
module.exports = {
    // Build Array once (on startup)
    buildArray: () => {
        const size = rsc.GetInternSizeList();
        let tmpArray = []; // init new one instead

        // Build the Array
        for (i = 0; i < size; i++)
            tmpArray.push(i);

        ArrayLock = shuffle(tmpArray);
        console.log(ArrayLock);
    },

    // Positions
    GetArrayLock: () => {
        return ArrayLock;
    },

    GetNameByPos: (pos) => {
        return rsc.GetInternName(ArrayLock[pos]);
    },

    // Commonly used for swap
    GetPosByName: (name) => {
        const i = rsc.GetIndexByName(name);
        return ArrayLock.indexOf(i);
    },

    // Commonly used for swap
    GetPosByID: (id) => {
        const i = rsc.getIndexbyID(id);
        console.log(i);
        return ArrayLock.indexOf(i);
    },

    // NeverUsed for now
    GetTodayID: () => {
        return TodayID;
    },

    // Return String ID
    GetTodayMaster: () => {
        return GetIDByPos(TodayID)
    },
    // Return String ID
    GetTomorrowMaster: () => {
        let tmp = GetIDByPos(TodayID + 1);

        if (tmp === undefined) {
            HandleLimit();
            return GetTodayMaster();
        } else
            return tmp;
    },

    Increment: () => {
        TodayID += 1;
    },
};