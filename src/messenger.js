// ref : https://www.npmjs.com/package/node-schedule
const schedule = require('node-schedule');
const base = require('./base.js');

module.exports = {

    sendNotification: (message, msg) => {
        message.channel.send(msg);
    },
    
    sendChannelNotification: (message, chan, msg) => {
        message.client.channels.get(chan).send(msg);
    },

    sendNotificationReply: (message, msg) => {
        message.channel.reply(msg);
    },

    sendDelayedNotification: (message, diff, msg, startup) => {
        let rule = new schedule.RecurrenceRule();
        rule.dayOfWeek = [0, new schedule.Range(1, 5)];
        rule.hour = diff[0];
        rule.minute = diff[1];
        // Create Calendar Event
        schedule.scheduleJob(rule, () => {
            message.channel.send(msg);
            if (startup)
                base.Increment();
        });
    },

    sendChannelDelayedNotification: (message, diff, chan, msg, startup) => {
        let rule = new schedule.RecurrenceRule();
        rule.dayOfWeek = [0, new schedule.Range(1, 5)];
        rule.hour = diff[0];
        rule.minute = diff[1];
        // Create Calendar Eventw
        schedule.scheduleJob(rule, () => {
            message.client.channels.get(chan).send(msg);
            if (startup)
                base.Increment();
        });
    },
}