# Bot Washer Master

My Very First Discord bot Called WasherMaster. Coded in Node.js. 
Everyday at 8:50 an intern will be picked randomly to manage the dishwasher. it also has a reminder function at 13:45 & 16:30.

Using Libraries : 
* Discord.js-commando
* node-schedule
* underscore

# Todo
* [x] Rand And Lock Function
* [x] Active/Desactive Command
* [x] Implement User List
* [ ] Save previous data in case of shutdown
* [x] Improve Code
>   Implemented Yatch-Algorithm

* [x] Add Today And Tomorrow Function
* [ ] Improve text dictionary (jokes)
* [ ] Create Config file
* [ ] Add Commands : Print / Debug, Config

### Todo
> Improve Code regarding Next Flush
