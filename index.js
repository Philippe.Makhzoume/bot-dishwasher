// Documentation : https://dragonfire535.gitbooks.io/discord-js-commando-beginners-guide/content/using-multiple-args-in-one-command.html
const { CommandoClient }  = require(`discord.js-commando`);
const path = require('path');
const TOKEN = 'NTA3MjQxNTQ3OTE0MjgwOTYw.Drt3qw.l6Mb_WSysytwkAyesGfoYujM_W8'

const client = new CommandoClient({
    commandPrefix: '.',
    unknownCommandResponse: false,
    owner: '96232199577161728',
    disableEveryone: true
});

client.registry
    .registerDefaultTypes()
    .registerGroups([
        ['simple', 'Commands only allowed to admins'],
        ['owners', 'Commands only allowed to admins'],
        ['master', 'Commands only allowed to the master'],
        ['users', 'Commands allowed by anyones'],
    ])
    .registerDefaultGroups()
    .registerDefaultCommands()
    .registerCommandsIn(path.join(__dirname, './Commands'));

client.on('ready', () => {
    console.log('Ready To Work !');
    client.user.setActivity('WATCHING');
    // bot.channels.get('391931371787911168').send('Hello here!');
});

client.login(TOKEN);
