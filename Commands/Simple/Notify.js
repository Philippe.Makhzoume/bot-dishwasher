const commando = require('discord.js-commando');
const schedule = require('node-schedule'); // ref : https://www.npmjs.com/package/node-schedule
const rsc = require('../../src/ressources.js');
const messenger = require('../../src/messenger.js');
const _ = require('../../src/base.js');
const swaper = require('../../src/swaper.js');

class NotifyCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'notify',
            group: 'simple',
            memberName: 'notify',
            description: 'Notify',
            args: [{
                key: 'text',
                prompt: 'What text would you like the bot to say?',
                type: 'string',
                validate: text => {
                    if (text === 'test') {
                        let temp = ["Phil", "Razan"];
                        swaper.swapByName(temp);
                        return true;
                    }
                    return 'Message Content is above 200 characters';
                }
            }]
        });
    }

    async run(message, {
        text
    }) {
        if (!rsc.isActive())
            return message.reply("Not active... run .activate first");

        if (text) {
            console.log(text);
            messenger.sendChannelNotification(message, '509416491720900609', 'testing from the other side');
        }
    }
}

module.exports = NotifyCommand;
