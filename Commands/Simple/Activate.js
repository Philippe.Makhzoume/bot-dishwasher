const commando = require('discord.js-commando');
// const alg = require('../../alg.js');
const base = require('../../src/base.js');
const rsc = require('../../src/ressources.js');
const messenger = require('../../src/messenger.js');


class ActivateCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'activate',
            aliases: ['start', 'desactivate', 'shutdown', 'kill'],
            group: 'owners',
            memberName: 'activate',
            description: 'Activate or desactivate the Dishwasher'
        });
    }

    async run(message) {
        rsc.SetActiveState();
        if (rsc.isActive())
        {
            message.reply('Okay Cheif ! WasherMaster police activated (NEE-eu NEE-eu)');
            base.buildArray();
        }
        else
        {
            const jobs = schedule.scheduledJobs;
            if (jobs)
            {
                schedule.cancelJob();
                message.channel.send("Disabling me ? How dare do you ? Fine... canceling jobs.");
            }
        }

        const config = rsc.GetScheduleConfig();
        const chan = rsc.GetChannels();
        let msg = `Today's turn is <${base.GetTodayMaster()}> Tomorrow will be <${base.GetTomorrowMaster()}>!`;
        messenger.sendDelayedNotification(message, config.Morning.time, msg, true);
        //messenger.sendDelayedNotification(message, [12, 0], "Test Seconde Notification", false);
    }
}

module.exports = ActivateCommand;
