const commando = require('discord.js-commando');
const schedule = require('node-schedule'); // ref : https://www.npmjs.com/package/node-schedule
// const alg = require('../../alg.js');
const base = require('../../src/base.js');
const rsc = require('../../src/ressources.js');
const msg = require('../../src/messenger.js');
const swapper = require('../../src/swaper.js');

module.exports = class SwapCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'swap',
            group: 'simple',
            memberName: 'swap',
            description: 'Swap',
            args: [
                {
                    key: 'user',
                    prompt: 'Which user do you want to send the DM to?',
                    type: 'user'
                },
                {
                    key: 'user2',
                    prompt: 'Which user do you want to send the DM to?',
                    type: 'user'
                },

                // {
                //     key: 'content',
                //     prompt: 'What would you like the content of the message to be?',
                //     type: 'string'
                // }
            ]
        });
    }

    async run(message, { user, user2 }) {
        if (!rsc.isActive())
            return msg.sendNotificationReply(message, "Bot is not active");

        console.log(user.id, user2.id);
        let tmp = `@${user.id}`;
        let tmp2 = `@${user2.id}`;

        swapper.swapByID([ tmp, tmp2] );
        // message.delete();
        //return user.send(content);
        
    }
}

